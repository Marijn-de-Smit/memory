using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System;



[Serializable]
public class Data{
    public float x;
    public float y;
}





public class GameController : MonoBehaviour
{

    public AudioClip bling;
    AudioSource audioSrc;

    // Meuk
    Thread receiveThread;
    UdpClient client;
    public int port;
    public string lastReceivedUDPPacket = "";
    public string allReceivedUDPPackets = "";

    List<GameObject> Buttons = new List<GameObject>();

    
    public float xPos = 10.0f;
    public float yPos = 10.0f;

    private List<float> gemx = new List<float>();
    private List<float> gemy = new List<float>();

    //Slider
    public Slider sliderx , slidery;
  
    public float facktorx, facktory;
    

    
    
    



    [SerializeField]
    private Sprite bgImage;
    

    public Sprite[] puzzles;

    public List<Sprite> gamePuzzles = new List<Sprite>();
   
    public List<Button> btns = new List<Button>();

    private bool firtsGuess, secondGuess;

    private int countGuesses;
    private int countCorrectGuesses;
    private int gameGuesses;

    private int firstGeussIndex, secondGeussIndex;

    private string firstGeussPuzzle, secondGeussPuzzle;


// tracking 
    public bool press;
    public int pressXPos;
    public int pressYPos;



    void Awake() {
        puzzles = Resources.LoadAll<Sprite> ("sprites/sch");
    }



    void Start() {
        
        bling = Resources.Load<AudioClip> ("Bling");
        audioSrc = GetComponent <AudioSource> ();


        sliderx.onValueChanged.AddListener(delegate{ChangedValue();});
        slidery.onValueChanged.AddListener(delegate{ChangedValue();});
        

        // UDP Socket
        port = 5065;

        receiveThread = new Thread(new ThreadStart(ReceiveData));
        receiveThread.IsBackground = true;
        receiveThread.Start();



        GetButtons ();
        AddListeners();
        AddGamePuzzles();
        
        //Shuffle (gamePuzzles);
        gameGuesses = gamePuzzles.Count /2 ; 

    }

    void Update(){
        // Command this if tracking wont work.
        if(press == true){
            press = false;
            PickApuzzelVloer();
            Debug.Log(pressXPos);
        }

    }

    public void ChangedValue() { 
        facktorx = sliderx.value;
        facktory = slidery.value;
    }           

    void GetButtons() {
        GameObject[] objects = GameObject.FindGameObjectsWithTag ("PuzzleButton");

        for(int i = 0; i < objects.Length; i++) {
            btns.Add(objects[i].GetComponent<Button>());
            btns[i].image.sprite = bgImage;
            //Debug.Log(btns[i].transform.position);

            
                    }
    }

    void AddGamePuzzles(){
        int looper = btns.Count;
        int index =0;
        for (int i = 0; i < looper; i++) {
            if (index == looper / 2) {
                index =0;
            }
            gamePuzzles.Add(puzzles[index]);

            index++;

        }    
    }


    void AddListeners() {
        foreach (Button btn in btns) {
            btn.onClick.AddListener(() => PickApuzzel());

        }   
    }

    public void PickApuzzel() {
        string name = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name;
        Debug.Log("You are Clicking a Button named"+ name);
       
        


        if(!firtsGuess) {
            firtsGuess = true;
            firstGeussIndex = int.Parse(UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name);
            Debug.Log(firstGeussIndex);

            firstGeussPuzzle = gamePuzzles[firstGeussIndex].name;

            btns[firstGeussIndex].image.sprite = gamePuzzles[firstGeussIndex];


        }else if (!secondGuess) {
            secondGuess = true;
            secondGeussIndex = int.Parse(UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name);
            
            secondGeussPuzzle = gamePuzzles[secondGeussIndex].name;

            btns[secondGeussIndex].image.sprite = gamePuzzles[secondGeussIndex];
            countGuesses++;

            StartCoroutine(CheckIfThePuzzlesMatch());

            
        }
     
    }

    public void PickApuzzelVloer() {

         string name = pressXPos.ToString();
         Debug.Log(pressXPos);
         Debug.Log(pressYPos);
        
        
        
           
            

        if(!firtsGuess) {
            firtsGuess = true;
            
            firstGeussIndex = pressXPos + (pressYPos * 4);
            
            firstGeussPuzzle = gamePuzzles[firstGeussIndex].name;
            

            btns[firstGeussIndex].image.sprite = gamePuzzles[firstGeussIndex];


        }else if (!secondGuess) {
            secondGuess = true;
            secondGeussIndex = pressXPos + (pressYPos * 4);
            
            secondGeussPuzzle = gamePuzzles[secondGeussIndex].name;

            btns[secondGeussIndex].image.sprite = gamePuzzles[secondGeussIndex];
            countGuesses++;

            StartCoroutine(CheckIfThePuzzlesMatch());

            
        }
     
    }

    // UDP socket
    private void ReceiveData()
	{
        int gemxindex = 0;

		client = new UdpClient(port);
		while (true)
		{
			try
			{
				IPEndPoint anyIP = new IPEndPoint(IPAddress.Parse("127.0.0.1"), port);
				byte[] data = client.Receive(ref anyIP);

				string text = Encoding.UTF8.GetString(data);
				//Debug.Log(text);
				Data received = JsonUtility.FromJson<Data>(text);
				
				//print(">> " + text);
				lastReceivedUDPPacket = text;
				allReceivedUDPPackets = allReceivedUDPPackets + text;

				xPos = received.x;
				xPos *= 8f * facktorx;

                yPos = received.y;
                
				yPos *= -4f * facktory;
                
                

                Move.Target = new Vector2(xPos,yPos);

                if (gemxindex < 50) {

                    gemx.Add(xPos);
                    gemy.Add(Mathf.Abs(yPos));
                   
                    gemxindex++;
                
                } else {

                    gemx.Sort();
                    gemy.Sort();

                    float maxx = gemx[gemx.Count-1];
                    float minx = gemx[0];
                    
                    float maxy = gemy[gemy.Count-1];
                    float miny = gemy[0];
                    


                    if ( (maxx - minx) < 30 && (maxy - miny) < 30){

                            
                        Debug.Log($"Het werkt gem y: {gemy[0]}");
                        Debug.Log($"Het werkt gem x: {gemx[0]}");
                        if(gemx[0] < (1920/4)){
                            press = true;
                            pressXPos = 0;

                        }
                        else if(gemx[0] < ((1920/4) * 2)){
                            press = true;
                            pressXPos = 1;

                        }
                        else if(gemx[0] < ((1920/4) * 3)){
                            press = true;
                            pressXPos = 2;

                        }
                        else{
                            press = true;
                            pressXPos = 3;

                        }
                        // yas
                        if(gemy[0] < (888/4)){
                            press = true;
                            pressYPos = 0;

                        }
                        else if(gemy[0] < ((888/4) * 2)){
                            press = true;
                            pressYPos = 1;

                        }
                       
                        else{
                            press = true;
                            pressYPos = 2;

                        }
                        

                    }

                    gemxindex = 0;
                    gemx.Clear();
                    gemy.Clear();
                }
				
				

                
			}
			catch (Exception e)
			{
				print(e.ToString());
			}

                

            
		}
	}

    public void clickButton(Vector2 location) {

        
        


    }

    
    

    IEnumerator CheckIfThePuzzlesMatch(){

        yield return new WaitForSeconds(1f);

        if(firstGeussPuzzle == secondGeussPuzzle){
                

                audioSrc.PlayOneShot (bling, 1.0F);
                yield return new WaitForSeconds(.5f);

                btns[firstGeussIndex].interactable = false;
                btns[secondGeussIndex].interactable = false;

                btns[firstGeussIndex].image.color = new Color (0,0,0,0);
                btns[secondGeussIndex].image.color = new Color (0,0,0,0);


                CheckIfTheGameIsFinished();

            } else {

                yield return new WaitForSeconds(.5f);

                btns[firstGeussIndex].image.sprite = bgImage;
                btns[secondGeussIndex].image.sprite = bgImage;
            }

            yield return new WaitForSeconds(.5f);

            firtsGuess = secondGuess = false;
            



    }

    void CheckIfTheGameIsFinished() {
        countCorrectGuesses++;
        if (countCorrectGuesses == gameGuesses) {
            Debug.Log("Game Finished");
            Debug.Log("It took you " + countGuesses + " guess(es) to finish the game");

        }
    }

    /*
    void Shuffle(List<Sprite> list) {
        
        for(int i =0; i < list.Count; i++) {

            Sprite temp = list[i];
            int randomIndex = Random.Range(0, list.Count);
            list[i] = list[randomIndex];
            list[randomIndex] = temp;

        } 
    }
*/

}

