using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Move : MonoBehaviour
{

    public static Vector2 Target;
    public RectTransform Object;
    


    // Start is called before the first frame update
    void Start()
    {

        Object = GetComponent<RectTransform>();

    }

    // Update is called once per frame
    void Update()
    {
        Object.anchoredPosition = Target;
    
        //Debug.Log(Target.x);
        //Debug.Log(Target.y);

        
    }
}
